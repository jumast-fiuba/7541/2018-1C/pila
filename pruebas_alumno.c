#include "pila.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef int elemento_t;


/* ******************************************************************
 *                   PRUEBAS UNITARIAS ALUMNO
 * *****************************************************************/

void prueba_crear_pila(){
    printf("INICIO DE PRUEBAS PARA PILA VACIA\n");

    pila_t* pila = pila_crear();
    print_test("la pila se creo correctamente", pila != NULL);
    print_test("la pila recien creada esta vacia", pila_esta_vacia(pila));
    print_test("ver el tope de la pila recien creada devuelve NULL", pila_ver_tope(pila) == NULL);
    print_test("desapilar de la pila recien creada devuelve NULL", pila_desapilar(pila) == NULL);

    /* Pruebo que luego de desapilar en una pila vacía, puedo apilar y desapilar nuevamente,
     * y que al hacerlo obtengo el valor correcto.*/
    char elemento = 'e';
    pila_apilar(pila, &elemento);
    print_test("luego de desapilar en una pila vacia, puedo volver a apilar y desapilar", pila_desapilar(pila) == &elemento);

    pila_destruir(pila);
}


void prueba_apilar_un_elemento(){
    printf("INICIO DE PRUEBAS PARA APILAR UN ELEMENTO\n");

    pila_t* pila = pila_crear();
    print_test("la pila se creo correctamente", pila != NULL);

    int valor = 5;
    print_test("se puede apilar el primer elemento", pila_apilar(pila, &valor));
    print_test("la pila no esta vacia luego de apilar el primer elemento", !pila_esta_vacia(pila));
    print_test("el tope es el elemento que se acaba de apilar", pila_ver_tope(pila) == &valor);
    print_test("desapilar devuelve el elemento que se acaba de apilar", pila_desapilar(pila) == &valor);
    print_test("la pila esta vacia luego de desapilar el elemento apilado", pila_esta_vacia(pila) == true);

    pila_destruir(pila);
}

void prueba_apilar_varios_elementos_poco_volumen(){
    printf("INICIO DE PRUEBAS PARA APILAR VARIOS ELEMENTOS - POCO VOLUMEN\n");

    pila_t* pila = pila_crear();
    print_test("la pila se creo correctamente", pila != NULL);

    /* Pruebo que puedo apilar 3 elementos */
    int valor1 = 1;
    char valor2[] = "dos";
    int valor3 = 3;
    bool ok1 = pila_apilar(pila, &valor1);
    bool ok2 = pila_apilar(pila, &valor2);
    bool ok3 = pila_apilar(pila, &valor3);
    print_test("se pueden apilar 3 elementos", ok1 && ok2 && ok3);

    print_test("luego de apilar los elementos la pila no esta vacia", pila_esta_vacia(pila) == false);
    print_test("el tope es el ultimo elemento apilado", pila_ver_tope(pila) == &valor3);
    print_test("al desapilar se obtienen los valores correctos",
               (pila_desapilar(pila) == &valor3) &&
               (pila_desapilar(pila) == &valor2) &&
               (pila_desapilar(pila) == &valor1));

    print_test("luego de desapilar los elementos que se apilaron, la pila esta vacia", pila_esta_vacia(pila) == true);
    print_test("luego de desapilar los elementos que se apilaron, ver_tope devuelve NULL", pila_ver_tope(pila) == NULL);
    print_test("luego de desapilar los elementos que se apilaron, pila_desapilar devuelve NULL", pila_desapilar(pila) == NULL);

    pila_destruir(pila);
}

void prueba_apilar_varios_elementos_mucho_volumen(){

    // Para que estas pruebas tengan sentido, la cantidad de elementos que se
    // apilan debe ser tal que se fuercen varios redimensionamientos,
    const size_t cantidad_de_elementos = 15000;

    printf("INICIO DE PRUEBAS PARA APILAR VARIOS ELEMENTOS - MUCHO VOLUMEN\n");

    pila_t* pila = pila_crear();
    print_test("la pila se creo correctamente", pila != NULL);

    // Creo un vector auxiliar con los elementos que voy a apilar
    // (o mejor dicho, con los elementos cuya dirección en memoria voy a apilar)
    elemento_t *elementos = malloc(cantidad_de_elementos * sizeof(elemento_t));
    for (size_t i = 0; i < cantidad_de_elementos; i++){
        elementos[i] = (elemento_t)i;
    }
    int tope = elementos[cantidad_de_elementos - 1];

    /* Pruebo que puedo apilar todos los elementos */
    size_t cant_apilados = 0;
    for (size_t i = 0; i < cantidad_de_elementos; i++){
        if(pila_apilar(pila, &elementos[i])){
            cant_apilados++;
        }
    }
    print_test("se apilaron todos los elementos", cant_apilados == cantidad_de_elementos);
    print_test("el tope es el ultimo elemento apilado", *(elemento_t*)pila_ver_tope(pila) == tope);

    /* Pruebo que al desapilar obtengo los valores correctos */
    bool desapila_ok = true;
    size_t cant_desapilados = 0;

    size_t i = cantidad_de_elementos - 1;
    while(!pila_esta_vacia(pila)){
        cant_desapilados++;
        if(pila_desapilar(pila) != &elementos[i--]){
            desapila_ok = false;
        }
    }
    print_test("se desapilaron todos los elementos", cant_desapilados == cantidad_de_elementos);
    print_test("al desapilar se obtienen los valores correctos", desapila_ok);


    pila_destruir(pila);
    free(elementos);
}

void pruebas_pila_alumno() {
    prueba_crear_pila();
    prueba_apilar_un_elemento();
    prueba_apilar_varios_elementos_poco_volumen();
    prueba_apilar_varios_elementos_mucho_volumen();
}